const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmdg12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountV2(req, res) {
 console.log("GET /apitechu/v2/accounts/:userid");

 var id = Number.parseInt(req.params.userid);
 console.log("La id de usuario para cuentas es " + id);
 var query = "q=" + JSON.stringify({"idUser": id});
 console.log("Query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("account?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     console.log(body);
     if (err) {
       response = {
         "msg" : "Error obteniendo cuentas."
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         response = body;
       } else {
         response = {
           "msg" : "El usuario no tiene cuentas."
         };
         res.status(404);
       }
     }
     res.send(response);
   }
 );
}

module.exports.getAccountV2 = getAccountV2;
