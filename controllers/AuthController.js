const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechujmdg12ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1(req, res) {
  console.log("LOGIN /apitechu/v1/users");

  /*Aquí cogemos el usuario del body */
      console.log(req.body);
      console.log(req.body.email);
      console.log(req.body.password);

      /*Recogemos los datos de login */
      var login = {
        "email" : req.body.email,
        "password" : req.body.password
    }

    var users = require('../usuarios.json');
    var i = 0;
    var logado = false;

    /*Buscamos el email */

    for (var i = 0; i < users.length; i++) {

      console.log("Entra en bucle");
      console.log("Email " + users[i].email);

      if (req.body.email == users[i].email &&
              req.body.password == users[i].password) {
          console.log("Email encontrado");
          console.log("Id de usuario " + users[i].id);
          users[i].logged = true;
          console.log("User logged" + users[i].logged);
          logado = true;
          break;
      }else if
        (i == users.length) {
          console.log("Usuario no encontrado");
        }
      }

      if (logado == true) {

      /*llamada a la funcion que escribe el fichero */
      io.writeUserDataToFile(users);

      res.send({"msg":"login correcto", "idUsuario": + users[i].id});
    } else if (logado == false) {
      res.send({"msg":"login incorrecto"});
    }

}

/* login v2 hecho por mi
function loginV2(req, res) {
  console.log("LOGIN /apitechu/v2/login/");

  var email = req.body.email;
  var password = req.body.password;
  console.log("El usuario a obtener es " + email);

  var query = "q={'email':'"+ email +"'}";

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {
        if (err) {
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            console.log("Usuario encontrado");
            var response = body[0];
            var passbd = body[0].password;
            console.log("password " + passbd + password);
            var logged = crypt.checkPassword(password, passbd);
            console.log("Logged " + logged);

            if (logged == true) {
              var id = body[0].id;
              var query = 'q={"id":'+ id +'}';
              var putBody = '{"$set":{"logged":true}}';
              console.log("query " + query);
              console.log("putBody " + putBody);
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(err, resMlab, body) {
                  console.log("Usuario logado");
              }
            );

          } else if (logged == false) {
            res.send({"msg":"login incorrecto"});
            }

          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
      }


      res.send(response);
    }
  )
}
*/

function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");

 var query = "q=" + JSON.stringify({"email": req.body.email});
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLABUrl);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     var isPasswordcorrect =
       crypt.checkPassword(req.body.password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);

     if (!isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401);
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       var id = Number.parseInt(body[0].idUser);
       query = "q=" + JSON.stringify({"idUser": id});
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUser" : body[0].idUser
           }
           res.send(response);
         }
       )
     }
   }
 );
}

function logoutV1(req, res) {
  console.log("LOGOUT /apitechu/v1/logout/:id");

  /*Aquí cogemos el id por parámetro */
      console.log("Parametro de entrada " + req.params.id);


    var users = require('../usuarios.json');
    var i = 0;
    var deslogado = false;

    /*Buscamos el email */

    for (var i = 0; i < users.length; i++) {

      console.log("Entra en bucle");
      console.log("id usuario " + users[i].id);
      console.log("logged " + users[i].logged);

      if (req.params.id == users[i].id && users[i].logged == true) {
          console.log("id encontrado");
          console.log("Id de usuario " + users[i].id);
          delete users[i].logged;
          deslogado = true;
          break;
      }else if
        (i == users.length) {
          console.log("Usuario no encontrado");
        }
      }

      if (deslogado == true) {

      /*llamada a la funcion que escribe el fichero */
      io.writeUserDataToFile(users);

      res.send({"msg":"logout correcto", "idUsuario": + users[i].id});
    } else if (deslogado == false) {
      res.send({"msg":"logout incorrecto"});
    }

}

/*Logout V2 hecho por mi
function logoutV2(req, res) {
  console.log("LOGOUT /apitechu/v2/logout/:id");

  var id = req.params.id;
  console.log("La id del usuario a obtener es " + id);

  var query = 'q={"id":'+ id +'}';

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {
        if (err) {
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0 && body[0].logged === true) {
            var response = body[0];
            var putBody = '{"$unset":{"logged":""}}'
            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err, resMlab, body) {
                console.log("Se ha hecho logout del usuario");
              }
            );
          } else {
            var response = {
              "msg" : "Error al realizar el Logout"
            }
            res.status(404);
          }
        }
      res.send(response);
    }
  )



}
*/

function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");
 var id = Number.parseInt(req.params.id);
 var query = "q=" + JSON.stringify({"idUser": id});
 console.log("query es " + query);
 httpClient = requestJson.createClient(baseMLABUrl);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       id = Number.parseInt(body[0].id);
       query = "q=" + JSON.stringify({"idUser": id});
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUser" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}



module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;

module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
