const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechujmdg12ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getMovementById(req, res) {
  console.log("GET /apitechu/v2/movements/:iban");

  var iban = req.params.iban;
  console.log("La cuenta a obtener sus movimientos es " + iban);

  var query = "q=" + JSON.stringify({"IBAN": iban});
  console.log("query es " + query);

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("movement?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {
      /* refactorizamos esto, pero sería lo que tendríamos que coger de ejemplo
      para el resto de funciones por que es lo mas sencillo
      var response = !err ? body[0] : {
        "msg" : "Error obteniendo usuario"
        */
        if (err) {
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            var response = body;
          } else {
            var response = {
              "msg" : "No se han encontrado movimientos"
            }
            res.status(404);
          }
        }
      res.send(response);
    }
  )
}

function createMovement(req, res) {
  console.log("POST /apitechu/v2/movements");

  console.log(req.body.IBAN);
  console.log(req.body.date);
  console.log(req.body.movement_type);
  console.log(req.body.amount);

    var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1;
  var yyyy = hoy.getFullYear();
  date = yyyy + '-' + mm + '-' + dd;

  console.log("La fecha es " + date)

  var newMovement = {
    "IBAN" : req.body.movement.IBAN,
    "movement_type" : req.body.movement_type,
    "date" : date,
    "amount" : req.body.amount
  }

  console.log(newMovement);


  var httpClient = requestJson.createClient(baseMLABUrl);
  httpClient.post("movement?" + mLabAPIKey, newMovement,
    function(err, resMlab, body) {
      console.log("Movimiento creado");
      res.status(201).send({"msg":"Movimiento creado"});
    }
  );

}


function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}


module.exports.getMovementById = getMovementById;
module.exports.createMovement = createMovement;
