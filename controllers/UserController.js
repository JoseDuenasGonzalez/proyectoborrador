const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechujmdg12ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../usuarios.json');

  if (req.query.$count == "true") {
    console.log("Count needed");
    result.count = users.length;
  }

  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

  res.send(result);

}

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLABUrl);

  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMlab, body) {
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios"
        }

        res.send(response);
    }

  );

}

function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a obtener es " + id);

  var query = "q=" + JSON.stringify({"idUser": id});
  console.log("query es " + query);

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {
      /* refactorizamos esto, pero sería lo que tendríamos que coger de ejemplo
      para el resto de funciones por que es lo mas sencillo
      var response = !err ? body[0] : {
        "msg" : "Error obteniendo usuario"
        */
        if (err) {
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            var response = body[0];
          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
      res.send(response);
    }
  )
}

function createUserV1(req, res) {
  console.log("POST /apitechu/v1/users");

/*Aquí cogíamos el usuario de las cabeceras
  console.log(req.headers);
  console.log(req.headers.first_name);
  console.log(req.headers.last_name);
  console.log(req.headers.email); */

  /*Recogemos los datos de un nuevo usuario */
  /*
  var newUser = {
    "first_name" : req.headers.first_name,
    "last_name" : req.headers.last_name,
    "email" : req.headers.email */


/*Aquí cogemos el usuario del body */
    console.log(req.body);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    /*Recogemos los datos de un nuevo usuario */
    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email
  }

  console.log(newUser);

  var users = require('../usuarios.json');
  users.push(newUser); /*añade algo al final de un string */

  console.log("Usuario añadido al array");

  /*llamada a la funcion que escribe el fichero */
  io.writeUserDataToFile(users);

  res.send({"msg":"usuario creado"});
}

function createUserV2(req, res) {
  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  }


  var httpClient = requestJson.createClient(baseMLABUrl);
  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMlab, body) {
      console.log("Usuario creado");
      res.status(201).send({"msg":"Usuario creado"});
    }
  );

}

function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("La id del usuario a borrar es " + req.params.id);

  var users = require('../usuarios.json');
  var i = 0;
  var borrado = false;
  var sum = 0;
  var element = 0;
  var value = 0;

  /*Practica borrado con bucle opcion 1 */

  for (var i = 0; i < users.length; i++) {

    console.log("entra en bucle");
    console.log("Id de usuario " + users[i].id);

    if (req.params.id == users[i].id) {
        console.log("Usuario encontrado");
        console.log("Id de usuario " + users[i].id);
        borrado = true
        break;
}else if
      (i == users.length) {
        console.log("Usuario no encontrado");
      }
    }


 /*Practica borrado con bucle opcion 2

  for (var i in users) {
    console.log("Usuario " + users[i].id);
    if (req.params.id == users[i].id) {
        console.log("Usuario encontrado");
        console.log("Id de usuario " + users[i].id);
        borrado = true
        break;
}else if
      (i == users.length) {
        console.log("Usuario no encontrado");
      }
  }

*/

 /*Practica borrado con bucle opcion 3

 // console.log("Usando for of");
    // for (user of users) {
    //   console.log("comparando " + user.id + " y " +  req.params.id);
    //   if (user.id == req.params.id) {
    //     console.log("La posición ? coincide");
    //     // Which one to delete? order is not guaranteed...
    //     deleted = false;
    //     break;
    //   }
    // }


*/

 /*Practica borrado con bucle opcion 4

 for (let value of users) {
  console.log(value.id);
  if (req.params.id == value.id) {
      console.log("Usuario encontrado");
      console.log("Id de usuario " + value.id);
      borrado = true
      break;
  }else if
    (i == users.length) {
      console.log("Usuario no encontrado");
    }
    i +=1
}

*/



  /*Con splice le decimos que quite del valor que nos han pasado
  por parámetro -1 (req.params.id -1) y que quite 1 */
  if (borrado == true) {
  users.splice(i, 1);

  /*llamada a la funcion que escribe el fichero */
  io.writeUserDataToFile(users);

  res.send({"msg":"usuario borrado"});
} else if (borrado == false) {
  res.send({"msg":"no encontrado usuario a borrar"});
 }
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
