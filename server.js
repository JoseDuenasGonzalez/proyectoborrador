require('dotenv').config();
const express = require('express');
const app = express(); /* inicializa el framework */

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 // This will be needed.
 res.set("Access-Control-Allow-Headers", "Content-Type");
 // Si usamos token habría que ponerlo aquí

 next();
}


app.use(express.json());
app.use(enableCORS);



const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
const movementController = require('./controllers/MovementController');

const port  = process.env.PORT || 3000; /* variable de entorno */

app.listen(port); /* pone en marcha el servidor */

console.log ("API escuchando en el puerto " + port);

app.get("/apitechu/v1/hello",
  function(req, res) {
    console.log("GET /apitechu/v1/hello");

    res.send({"msg":"Hola desde API TechU"});
  }
)

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }

)

/*Consulta basica de usuarios
app.get("/apitechu/v1/users",
  function(req, res) {
    console.log("GET /apitechu/v1/users");

    var users = require('./usuarios.json');
    res.send(users);

    otra forma de hacer las dos lineas anteriores
    res.sendFile("usuarios.json", {root: __dirname}); */

app.get("/apitechu/v1/users", userController.getUsersV1);

app.get("/apitechu/v2/users", userController.getUsersV2);

app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);

app.post("/apitechu/v1/users", userController.createUserV1);

app.post("/apitechu/v2/users", userController.createUserV2);

app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);

app.post("/apitechu/v1/login", authController.loginV1);

app.post("/apitechu/v2/login", authController.loginV2);

app.post("/apitechu/v1/logout/:id", authController.logoutV1);

app.post("/apitechu/v2/logout/:id", authController.logoutV2);

app.get("/apitechu/v2/accounts/:userid", accountController.getAccountV2);
app.get("/apitechu/v2/movements/:iban", movementController.getMovementById);
app.get("/apitechu/v2/movements", movementController.createMovement);
